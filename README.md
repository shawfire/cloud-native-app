# Cloud Native App

<details><summary>1. Get test data</summary>

   [Using the Google Books API](https://developers.google.com/books/docs/v1/using)

```javascript
    let data;
    fetch('https://www.googleapis.com/books/v1/volumes?q=golang&orderBy=newest').then(res => res.json()).then(d => data = d)

    fetch('https://www.googleapis.com/books/v1/volumes?q=golang&orderBy=newest')
      .then(res => res.json()).then(d => data = d)

    data.items.map(b => { return { id: b.id, title: b.volumeInfo.title, authors: b.volumeInfo.authors.join(', '), isbn: b.volumeInfo.industryIdentifiers.filter(i => i.type === "ISBN_13")[0].identifier.replace(/^(\d{3})(\d{1})(\d{2})(\d{6})(\d{1})$/,'$1-$2-$3-$4-$5'), "publishedDate": b.volumeInfo.publishedDate, description: b.volumeInfo.description  }; })

    data.items.map(b => { 
      return { 
        id: b.id, 
        title: b.volumeInfo.title, 
        authors: b.volumeInfo.authors.join(', '), 
        isbn: b.volumeInfo.industryIdentifiers.filter(i => i.type === "ISBN_13")[0]
          .identifier.replace(/^(\d{3})(\d{1})(\d{2})(\d{6})(\d{1})$/,'$1-$2-$3-$4-$5'), 
        publishedDate: b.volumeInfo.publishedDate,
        description: b.volumeInfo.description,  }; })
```
</details>

<details><summary>2. Golang micro-service</summary>

```bash
    cloud-native-app/go-api/src $ go run microservice.go
    $ curl "localhost:8080"
    Welcome to my Cloud Native Go Microservice.
    
    cloud-native-app/go-api/src $ export AUTHOR="John Shaw"
    cloud-native-app/go-api/src $ export PORT=8001
    cloud-native-app/go-api/src $ go run microservice.go
    $ curl "localhost:8001"
    Welcome to John Shaw's Cloud Native Go Microservice.

    cloud-native-app/go-api/src $ go build -o microservice microservice.go
    cloud-native-app/go-api/src $ ./microservice
    $ unset AUTHOR && unset PORT
    $ curl "localhost:8080/hello?name=Susie"
    Hello Susie, I hope you are enjoying my microservice!

    cloud-native-app/go-api/src $ export GOPATH=`pwd`
    cloud-native-app/go-api/src $ go build -o microservice
    cloud-native-app/go-api/src $ ./microservice

    cloud-native-app/go-api/src $ go get github.com/stretchr/testify/assert
    cloud-native-app/go-api/src $ go test ./api -v
    === RUN   TestBookToJSON
    --- PASS: TestBookToJSON (0.00s)
    === RUN   TestBookFromJSON
    --- PASS: TestBookFromJSON (0.00s)
    PASS
    ok      _/Users/shawfire/go/src/goTalk/cloud-native-app/go-api/src/api  0.013s

    Use postman instead of curl "localhost:8080/books". Create a new postman collection and associated environment.
```
```json
    Add POST to add a Book in postman with payload:

    {
      "authors":"Arpit Aggarwal",
      "description":"86 recipes on how to build fast, scalable, and powerful web services and applications with Go Key Features Become proficient in RESTful web services Build scalable, high-performant web applications in Go Get acquainted with Go frameworks for web development Book Description Go is an open source programming language that is designed to scale and support concurrency at the language level. This gives you the liberty to write large concurrent web applications with ease. From creating web application to deploying them on Amazon Cloud Services, this book will be your one-stop guide to learn web development in Go. The Go Web Development Cookbook teaches you how to create REST services, write microservices, and deploy Go Docker containers. Whether you are new to programming or a professional developer, this book will help get you up to speed with web development in Go. We will focus on writing modular code in Go; in-depth informative examples build the base, one step at a time. You will learn how to create a server, work with static files, SQL, NoSQL databases, and Beego. You will also learn how to create and secure REST services, and create and deploy Go web application and Go Docker containers on Amazon Cloud Services. By the end of the book, you will be able to apply the skills you've gained in Go to create and explore web applications in any domain. What you will learn Create a simple HTTP and TCP web server and understand how it works Explore record in a MySQL and MongoDB database Write and consume RESTful web service in Go Invent microservices in Go using Micro – a microservice toolkit Create and Deploy the Beego application with Nginx Deploy Go web application and Docker containers on an AWS EC2 instance Who this book is for This book is for Go developers interested in learning how to use Go to build powerful web applications. A background in web development is expected.",
      "id":"E99YDwAAQBAJ",
      "isbn":"978-1-78-728856-0",
      "publishedDate":"2018-04-23",
      "title":"Go Web Development Cookbook"
    }

    GET book by isbn in postman:
    localhost:{{port}}/books/978-1-78-862253-0

    DELETE book by isbn in postman:
    localhost:{{port}}/books/978-1-78-728856-0

    PUT request to update the contents of a book in postman:
    localhost:{{port}}/books/978-1-78-728856-0
```
</details>

<details><summary>3. Docker</summary>

[Public Repository of Docker Images](hub.docker.com) - Search for "golang", shawfire/T...

```bash
$ docker images
REPOSITORY                                              TAG                 IMAGE ID            CREATED             SIZE
128553428792.dkr.ecr.us-east-1.amazonaws.com/shawfire   john                3fe34eb4f087        3 weeks ago         8.35MB
shawfire                                                john                3fe34eb4f087        3 weeks ago         8.35MB
alpine                                                  3.7                 791c3e2ebfcb        4 weeks ago         4.2MB
$ docker pull golang:1.8-alpine

```
FROM golang:1.8
MAINTAINER John Shaw

ENV SOURCES /go/src/github.com/shawfire/go-microservice/

COPY . ${SOURCES}

RUN cd ${SOURCES} && CGO_ENABLED=0 && go install

ENV PORT 8001
EXPOSE 8001

ENTRYPOINT [ "go-miscroservice" ]
```
```bash
$ docker build -t go-microservice:1.0.0 .
$ docker images
( around 680 MB)
Smaller image:
FROM golang:1.8-alpine
$ docker build -t go-microservice:1.0.1 .
$ docker images
(around 250 MB)
$ docker tag go-microservice:1.0.1 shawfire/go-microservice:1.0.1
$ docker images
$ docker push shawfire/go-microservice:1.0.1
https://hub.docker.com

$ docker run -it -p 8080:8080 go-microservice:1.0.1 
(-it: iteractive terminal, docker port: host port)

$ docker run -it -e "PORT=8001" -p 8001:8001 go-microservice:1.0.1 

$ docker -ps --all
$ docker run --name go-microservice -d -p 8080:8080 go-microservice:1.0.1
$ docker ps
$ docker stats
$ docker stop go-microservice
$ docker kill go-microservice
$ docker rm go-microservice
$ docker -ps --all (one less because of docker rm ...)
$ docker run --name go-microservice --cpu-quota 50000 --memory 64m --memory-swappiness 0 -d -p 8080:8080 go-microservice:1.0.1
$ docker ps
```

```docker
FROM alpine:3.5
MAINTAINER John Shaw

COPY ./go-microservice /app/go-microservice

RUN chmod +x /app/go-microservice

ENV PORT 8001
EXPOSE 8001

ENTRYPOINT /app/go-miscroservice
```

```bash
$ GOOS=linux GOARCH=amd64 go build
$ ls -al
$ docker build -t go-microservice:1.0.1-alpine .
$ docker images
(around 15 MB)
$ docker tag go-microservice:1.0.1-alpine shawfire/go-microservice:1.0.1-alpine
$ docker images
$ docker push shawfire/go-microservice:1.0.1-alpine

$ docker-compose build
$ docker-compose up -d
$ docker ps
https://...:8080 (nginx)
https://...:9090 (api)
$ docker-compose.exe kill
$ docker-compose.exe rm

```

```
cloud-native-app/go-api/src $ GOOS=linux GOARCH=amd64 go build -o go-microservice
cloud-native-app $ cd ../..
cloud-native-app $ mv src/go-api/go-microservice .
cloud-native-app $ docker build -t go-microservice:1.0.1-alpine .
cloud-native-app $ docker images
REPOSITORY                                              TAG                            IMAGE ID            CREATED             SIZE
go-microservice                                         1.0.1-alpine                   7ea4ade94ff8        16 minutes ago      16.7MB
cloud-native-app $ docker tag go-microservice:1.0.1-alpine shawfire/go-microservice:1.0.1-alpine
$ docker images
REPOSITORY                                              TAG                 IMAGE ID            CREATED             SIZE
shawfire/go-microservice                                1.0.1-alpine        7ea4ade94ff8        18 minutes ago      16.7MB
cloud-native-app $ docker push shawfire/go-microservice:1.0.1-alpine
cloud-native-app $ docker run --name go-microservice -d -p 8080:8080 go-microservice:1.0.1-alpine
cloud-native-app $ docker ps
```

</details>

<details><summary>4. Kubernetes</summary>
</details>

<details><summary>5. React.js</summary>

</details>
