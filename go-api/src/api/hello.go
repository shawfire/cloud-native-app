package api

import (
	"fmt"
	"net/http"
)

// Hello is a HandleFunc which uses the "name" query parameter and
// an Author func toreturn a message for the route "hello"
func HelloHandleFunc(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query()["name"][0]
	w.Header().Add("Content-Type", "text/plain")
	fmt.Fprintf(w, 
		"Hello %s, I hope you are enjoying %s microservice!\n", 
		name, Author())
}
