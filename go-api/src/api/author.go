package api

import "os"

// Author returns the environment variable AUTHOR's name 
// or "my" as the default.
func Author() string {
	author := os.Getenv("AUTHOR")
	if len(author) == 0 {
		author = "my"
	} else {
		author += "'s"
	}
	return author
}