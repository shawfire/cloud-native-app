package api

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBookToJSON(t *testing.T) {
	book := Book{
		ID: "gvBZDwAAQBAJ", 
		Title: "Mastering Go", 
		Authors: "Mihalis Tsoukalos", 
		Isbn: "978-1-78-862253-0", 
		PublishedDate: "2018-04-30",
	}
	json := book.ToJSON()

	assert.Equal(t, 
		`{"id":"gvBZDwAAQBAJ","title":"Mastering Go","authors":"Mihalis Tsoukalos","isbn":"978-1-78-862253-0","publishedDate":"2018-04-30"}`,
		string(json), "Book JSON marshalling wrong.")
}

func TestBookFromJSON(t *testing.T) {
	json := []byte(`{"id":"gvBZDwAAQBAJ","title":"Mastering Go","authors":"Mihalis Tsoukalos","isbn":"978-1-78-862253-0","publishedDate":"2018-04-30"}`)
	book := FromJSON(json)
	assert.Equal(t, 
		Book{
			ID: "gvBZDwAAQBAJ", 
			Title: "Mastering Go", 
			Authors: "Mihalis Tsoukalos", 
			Isbn: "978-1-78-862253-0", 
			PublishedDate: "2018-04-30",
		}, 
		book, "Book JSON unmarshalling wrong.")
}
