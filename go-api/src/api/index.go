package api

import (
	"fmt"
	"net/http"
)

func IndexHandleFunc(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, 
		"Welcome to %s Cloud Native Go Microservice.\n", 
		Author())
}
