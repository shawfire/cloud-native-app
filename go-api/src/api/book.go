package api

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
)

// Book type with Name, Author and ISBN
type Book struct {
	ID       			string `json:"id"`
	Title       	string `json:"title"`
	Authors      	string `json:"authors"`
	Isbn        	string `json:"isbn"`
	PublishedDate	string `json:"publishedDate"`
	Description		string `json:"description,omitempty"`
}

var books = map[string]Book{
	"978-1-78-862253-0": Book{
		ID: "gvBZDwAAQBAJ", 
		Title: "Mastering Go", 
		Authors: "Mihalis Tsoukalos", 
		Isbn: "978-1-78-862253-0", 
		PublishedDate: "2018-04-30",
		Description: "Exploring the major features and packages of Go, along with its types and data-structures, enabling the reader to write threadsafe, concurrent cloud, and network applications Key Features Not your typical introduction to the Golang programming language Exploring Golang cradle to grave, completes the developer’s Golang education A thorough exploration into the core libraries and Golang features, that usually are taken for granted In depth explanation, detailing the rationale behind composite data types, Golang concurrency, and the Golang networking library Book Description The Go programming language, often referred to as Golang (albeit wrongly), is really making strides, with some masterclass developments, architected by the greatest programming minds. Tobias Lutke, CEO of Shopify, recently quoted as saying “Go will be the server language of the future”, powerful words, with much ambition. Go programmers are in high demand, but more controversially, Go takes the stage, where C and Unix programmers previously led the way. The growth of the Go language has seen it become the means by which systems, networking, web, and cloud applications are implemented. Comfortable with syntax, you’ll benefit by mastering the use of the libraries and utilise its features, speed, and efficiency, for which the Go ecology is justly famous. You already know a little Go syntax and you’ve written some small projects, most Go programmers face the difficulty of having to integrate their Golang skills with production code. Typical introductions to Go programming, often stop short of this transition, the author continue on, showing you just how to tackle this. Offering a compendium of Go, the book begins with an account of how Go has been implemented, also, the reader will benefit from a dedicated chapter, an in-depth account of concurrency, systems and network programming, imperative for modern-day native cloud development. What you will learn Understand the design choices of Golang syntax Know enough Go internals to be able to optimize Golang code Appreciate concurrency models available in Golang Understand the interplay of systems and networking code Write server-level code that plays well in all environments Understand the context and appropriate use of Go data types and data structures Who this book is for This book is for Golang programmers. You should have previously read an introductory book on Go, or to have worked through the Tour of Go or an equivalent online course. This book will definitely help to remember the basic concepts of concurrency, but network programming will be explained. A certain amount of previous coding and production experience would be helpful.",
	},
	"978-1-78-862965-2": Book{
		ID: "iLZTDwAAQBAJ",
		Title: "Rust Standard Library Cookbook",
		Authors: "Daniel Durante, Jan Nils Ferner",
		Isbn: "978-1-78-862965-2",
		PublishedDate: "2018-03-29",
		Description: "Explore the Rust Standard library and compose algorithms with minimal dependency on external libraries Key Features Develop high-quality, fast, and portable applications by leveraging the power of Rust's Standard library. Practical recipes that will help you work with the Standard library to boost your productivity as a Rust developer. Learn about most relevant external crates to be used along with the Standard library. Book Description Mozilla’s Rust is gaining much attention with amazing features and a powerful library. This book will take you through varied recipes to teach you how to leverage the Standard library to implement efficient solutions. The book begins with a brief look at the basic modules of the Standard library and collections. From here, the recipes will cover packages that support file/directory handling and interaction through parsing. You will learn about packages related to advanced data structures, error handling, and networking. You will also learn to work with futures and experimental nightly features. The book also covers the most relevant external crates in Rust. By the end of the book, you will be proficient at using the Rust Standard library. What you will learn How to use the basic modules of the library: strings, command line access, and more. Implement collections and folding of collections using vectors, Deque, linked lists, and more. Handle various file types , compressing and decompressing data. Search for files with glob patterns. Implement parsing through various formats such as CSV, TOML, and JSON. Utilize drop trait , the Rust version of destructor. Resource locking with Bilocks. Who this book is for This book is for developers who would like to explore the power of Rust and learn to use the STL for various functionalities. A basic Rust programming knowledge is assumed.",
	},
}

// ToJSON to be used for marshalling of Book type
func (b Book) ToJSON() []byte {
	ToJSON, err := json.Marshal(b)
	if err != nil {
		panic(err)
	}
	return ToJSON
}

// FromJSON to be used for unmarshalling of Book type
func FromJSON(data []byte) Book {
	book := Book{}
	err := json.Unmarshal(data, &book)
	if err != nil {
		panic(err)
	}
	return book
}

// AllBooks returns a slice of all books
func AllBooks() []Book {
	values := make([]Book, len(books))
	idx := 0
	for _, book := range books {
		values[idx] = book
		idx++
	}
	return values
}

func writeJSON(w http.ResponseWriter, i interface{}) {
	b, err := json.Marshal(i)
	if err != nil {
		panic(err)
	}
	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Write(b)
}

// CreateBook creates a new Book if it does not exist
func CreateBook(book Book) (string, bool) {
	_, exists := books[book.Isbn]
	if exists {
		return "", false
	}
	books[book.Isbn] = book
	return book.Isbn, true
}


// BooksHandleFunc to be used as http.HandleFunc for Book API
func BooksHandleFunc(w http.ResponseWriter, r *http.Request) {
	switch method := r.Method; method {
	case http.MethodGet:
		books := AllBooks()
		writeJSON(w, books)
	case http.MethodPost:
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
		book := FromJSON(body)
		isbn, created := CreateBook(book)
		if created {
			w.Header().Add("Location", "/api/books/"+isbn)
			w.WriteHeader(http.StatusCreated)
		} else {
			w.WriteHeader(http.StatusConflict)
		}
	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Unsupported request method."))
	}
}

// BookHandleFunc to be used as http.HandleFunc for Book API
func BookHandleFunc(w http.ResponseWriter, r *http.Request) {
	isbn := r.URL.Path[len("/books/"):]

	switch method := r.Method; method {
	case http.MethodGet:
		book, found := GetBook(isbn)
		if found {
			writeJSON(w, book)
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	case http.MethodPut:
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
		book := FromJSON(body)
		exists := UpdateBook(isbn, book)
		if exists {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	case http.MethodDelete:
		DeleteBook(isbn)
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Unsupported request method."))
	}
}

// GetBook returns the book for a given ISBN
func GetBook(isbn string) (Book, bool) {
	book, found := books[isbn]
	return book, found
}

// UpdateBook updates an existing book
func UpdateBook(isbn string, book Book) bool {
	_, exists := books[isbn]
	if exists {
		books[isbn] = book
	}
	return exists
}

// DeleteBook removes a book from the map by ISBN key
func DeleteBook(isbn string) {
	delete(books, isbn)
}
