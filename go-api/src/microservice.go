package main

/*
	Usage:
		cloud-native-app/go-api/src $ go run microservice.go
		cloud-native-app $ curl "localhost:8080"

		cloud-native-app/go-api/src $ export AUTHOR="John Shaw"
		cloud-native-app/go-api/src $ export PORT=8001
		cloud-native-app/go-api/src $ go run microservice.go
		cloud-native-app $ curl "localhost:8001"
*/

import (
	"net/http"
	"os"

	"./api"
)

func main() {
	http.HandleFunc("/", api.IndexHandleFunc)
	http.HandleFunc("/hello", api.HelloHandleFunc)
	http.HandleFunc("/books", api.BooksHandleFunc)
	http.HandleFunc("/books/", api.BookHandleFunc)
	http.ListenAndServe(port(), nil)
}

func port() string {
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "8080"
	}
	return ":" + port
}

