FROM alpine:3.8
MAINTAINER John Shaw

COPY ./go-microservice /app/go-microservice

RUN chmod +x /app/go-microservice

ENV PORT 8001
EXPOSE 8001

ENTRYPOINT /app/go-miscroservice